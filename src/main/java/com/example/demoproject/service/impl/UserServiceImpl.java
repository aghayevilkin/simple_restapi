package com.example.demoproject.service.impl;

import com.example.demoproject.dto.request.UserRequestDto;
import com.example.demoproject.dto.response.UserResponseDto;
import com.example.demoproject.exception.AuthorityAlreadyExistException;
import com.example.demoproject.exception.UserNotFoundId;
import com.example.demoproject.exception.UsernameAlreadyExistException;
import com.example.demoproject.model.Authority;
import com.example.demoproject.model.User;
import com.example.demoproject.repository.AuthorityRepository;
import com.example.demoproject.repository.UserRepository;
import com.example.demoproject.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final ModelMapper mapper;
    private final PasswordEncoder encoder;

    private final AuthorityRepository authorityRepository;

    @Override
    public UserResponseDto createUser(UserRequestDto request) {

        //check username
        userRepository.findUserByUsernameIgnoreCase(request.getUsername())
                .ifPresent(user -> {
                    throw new UsernameAlreadyExistException(request.getUsername());
                });

        //check authority
        Authority authority = authorityRepository.findByAuthorityNameIgnoreCase(request.getAuthority())
                .orElseThrow(()->new AuthorityAlreadyExistException(request.getAuthority()));

        User user = mapper.map(request,User.class);
        user.setPassword(encoder.encode(request.getPassword()));
        user.setAuthority(authority);
        System.out.println("create");
        return mapper.map(userRepository.save(user),UserResponseDto.class);
    }

    @Override
    public void deleteUser(Long id) {
        User user = userRepository.findById(id)
                .orElseThrow(()-> new UserNotFoundId(id));
        userRepository.delete(user);
    }

    @Override
    public UserResponseDto findById(Long id) {
        return userRepository.findById(id)
                .map((s) -> mapper.map(s, UserResponseDto.class))
                .orElseThrow(()-> new UserNotFoundId(id));
    }

}
