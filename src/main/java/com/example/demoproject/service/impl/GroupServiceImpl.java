package com.example.demoproject.service.impl;

import com.example.demoproject.dto.request.GroupRequestDto;
import com.example.demoproject.dto.response.GroupResponseDto;
import com.example.demoproject.exception.GroupNotFound;
import com.example.demoproject.exception.NotFoundTeacherId;
import com.example.demoproject.model.Group;
import com.example.demoproject.model.Teacher;
import com.example.demoproject.repository.GroupRepository;
import com.example.demoproject.repository.TeacherRepository;
import com.example.demoproject.service.GroupService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Slf4j
@Service
@RequiredArgsConstructor
public class GroupServiceImpl implements GroupService {

    private final ModelMapper mapper;
    private final GroupRepository groupRepository;
    private final TeacherRepository teacherRepository;

    @Override
    public GroupResponseDto createGroup(GroupRequestDto groupRequestDto) {
        //TODO bazadan yoxla ki hemin teacher var sende ya yox!  +
        Long teacherId = groupRequestDto.getTeacherId();
        Teacher teacher = teacherRepository.findById(teacherId).orElseThrow(()-> new NotFoundTeacherId(teacherId));// TODO getById ni findById ile deyisdir.. +
        Group group = mapper.map(groupRequestDto, Group.class);
        group.setTeacher(teacher);
        return mapper.map(groupRepository.save(group), GroupResponseDto.class);

    }
    @Transactional
    public void deleteGroup(Long id) {
        Group group = groupRepository.findById(id).orElseThrow(GroupNotFound::new);
        groupRepository.delete(group);
    }

    @Override
    public GroupResponseDto update(Long id, GroupRequestDto groupDto) {
        Group group = groupRepository.findById(id).orElseThrow(() -> new GroupNotFound());

        mapper.map(groupDto, group);

//        BeanUtils.copyProperties(studentdto, student);

        return mapper.map(groupRepository.save(group), GroupResponseDto.class);
    }


    @Override
    public GroupResponseDto findById(Long id) {
        return groupRepository.findById(id)
                .map((s) -> mapper.map(s, GroupResponseDto.class)).orElseThrow(GroupNotFound::new);
    }
}
