package com.example.demoproject.service.impl;

import com.example.demoproject.dto.request.TeacherRequestDto;
import com.example.demoproject.dto.response.TeacherResponseDto;
import com.example.demoproject.exception.TeacherNotFound;
import com.example.demoproject.model.Teacher;
import com.example.demoproject.repository.TeacherRepository;
import com.example.demoproject.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {

    private final TeacherRepository teacherRepository;
    private final ModelMapper mapper;

    @Override
    public TeacherResponseDto createTeacher(TeacherRequestDto teacherDto){
        Teacher teacher = mapper.map(teacherDto, Teacher.class);
        return mapper.map(teacherRepository.save(teacher), TeacherResponseDto.class);
    }

    @Override
    @Transactional
    public void deleteTeacher(Long id){
        Teacher teacher = teacherRepository.findById(id).orElseThrow(TeacherNotFound::new);
        teacherRepository.delete(teacher);
    }

    @Override
    public TeacherResponseDto update(Long id, TeacherRequestDto teacherRequestDto) {
        Teacher teacher = teacherRepository.findById(id).orElseThrow(() ->
                new TeacherNotFound());

        mapper.map(teacherRequestDto, teacher);

//        BeanUtils.copyProperties(studentdto, student);

        return mapper.map(teacherRepository.save(teacher), TeacherResponseDto.class);

    }

    @Override
    public TeacherResponseDto findById(Long id) {
        return teacherRepository.findById(id)
                .map((s) -> mapper.map(s, TeacherResponseDto.class))
                .orElseThrow(()-> new TeacherNotFound());
    }
}
