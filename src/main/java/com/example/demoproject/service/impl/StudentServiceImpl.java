package com.example.demoproject.service.impl;

import com.example.demoproject.dto.request.StudentRequestDto;
import com.example.demoproject.dto.response.StudentResponseDto;
import com.example.demoproject.exception.NotFoundGroupId;
import com.example.demoproject.exception.StudentNotFound;
import com.example.demoproject.model.Group;
import com.example.demoproject.model.Student;
import com.example.demoproject.repository.GroupRepository;
import com.example.demoproject.repository.StudentRepository;
import com.example.demoproject.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;
    private final ModelMapper mapper;

    @Override
    public StudentResponseDto createStudent(StudentRequestDto studentRequestDto) {
        Long groupId = studentRequestDto.getGroupId();
        Group group = groupRepository.findById(groupId).orElseThrow(()->new NotFoundGroupId(groupId));
        Student student = mapper.map(studentRequestDto,Student.class);
        student.setGroups(List.of(group));
//        Address address = mapper.map(studentdto.getAddress(), Address.class);

        return mapper.map(studentRepository.save(student), StudentResponseDto.class);
    }

    @Override
    @Transactional
    public void deleteStudent(Long id) {
        Student student = studentRepository.findById(id).orElseThrow(StudentNotFound::new);
        studentRepository.delete(student);
    }

    @Override
    public StudentResponseDto update(Long id, StudentRequestDto studentRequestDto) {
        Student student = studentRepository.findById(id).orElseThrow(StudentNotFound::new);

        mapper.map(studentRequestDto, student);
//        BeanUtils.copyProperties(studentdto, student);
        return mapper.map(studentRepository.save(student), StudentResponseDto.class);

    }

    @Override
    public StudentResponseDto findById(Long id) {
        return studentRepository.findById(id).map((s) -> mapper.map(s, StudentResponseDto.class)).orElseThrow(StudentNotFound::new);
    }
}
