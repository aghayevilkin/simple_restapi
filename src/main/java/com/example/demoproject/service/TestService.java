package com.example.demoproject.service;


import com.example.demoproject.model.Address;
import com.example.demoproject.model.Group;
import com.example.demoproject.model.Student;
import com.example.demoproject.model.Teacher;
import com.example.demoproject.repository.AddressRepository;
import com.example.demoproject.repository.GroupRepository;
import com.example.demoproject.repository.StudentRepository;
import com.example.demoproject.repository.TeacherRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class TestService {

    // no final keyword, but it running
    private final StudentRepository studentRepository;
    private final GroupRepository groupRepository;

    @Transactional
    public void bridge(){
        createtest();
    }
    public void createtest() {

        Address address = new Address();
        address.setCity("Gence");
        address.setStreet("R Kerimov");


        Student student = new Student();
        student.setAge(22);
        student.setEmail("example@gmail.com");
        student.setFirstName("kamil");
        student.setLastName("hesenli");
        student.setPhone("1234141");
        student.setAddress(address);
        studentRepository.save(student);

//        if (true){
//            throw new RuntimeException("EXCEPTION");
//        }

        Group group = new Group();
        group.setName("A");
        group.setDetails("test");
        groupRepository.save(group);





        /**
         * burada hem addressi hemde studenti save etsek gerek Cascate typeni persist etmeyek
         * yoxsa islemeyecek yada yanlizca studenti save edek bu sayede studentde gedib addressi
         * create edecek
         */



//        Group group = new Group();
//        group.setName("A");

//
//        student.setGroups(List.of(group));
////        groupRepository.save(group);
//            studentRepository.save(student);
//        log.info("save student *****");



//        addressRepository.save(address);
//        teacherReository.save(teacher);
//        studentRepository.deleteById(3l);

//        System.out.println("--------------------------");
//        studentRepository.findAll().stream()
//                .forEach((s) -> System.out.println(s));
    }
}
