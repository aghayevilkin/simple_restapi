package com.example.demoproject.service;

import com.example.demoproject.dto.request.UserRequestDto;
import com.example.demoproject.dto.response.UserResponseDto;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    UserResponseDto createUser(UserRequestDto request);

    void deleteUser(Long id);

    UserResponseDto findById(Long id);
}
