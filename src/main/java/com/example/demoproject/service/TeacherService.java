package com.example.demoproject.service;

import com.example.demoproject.dto.request.TeacherRequestDto;
import com.example.demoproject.dto.response.TeacherResponseDto;
import org.springframework.stereotype.Service;


@Service
public interface TeacherService {


    TeacherResponseDto createTeacher(TeacherRequestDto teacherDto);

    void deleteTeacher(Long id);

    TeacherResponseDto update(Long id, TeacherRequestDto teacherDto);

    TeacherResponseDto findById(Long id);
}
