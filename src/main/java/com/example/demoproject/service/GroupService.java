package com.example.demoproject.service;

import com.example.demoproject.dto.request.GroupRequestDto;
import com.example.demoproject.dto.response.GroupResponseDto;
import org.springframework.stereotype.Service;

@Service
public interface GroupService {


    GroupResponseDto createGroup(GroupRequestDto groupRequestDto);

    void deleteGroup(Long id);

    GroupResponseDto update (Long id, GroupRequestDto groupRequestDto);

    GroupResponseDto findById(Long id);
}
