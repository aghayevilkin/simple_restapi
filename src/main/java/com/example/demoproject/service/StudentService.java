package com.example.demoproject.service;

import com.example.demoproject.dto.request.StudentRequestDto;
import com.example.demoproject.dto.response.StudentResponseDto;

public interface StudentService {

    StudentResponseDto createStudent(StudentRequestDto studentRequestDto);

    void deleteStudent(Long id);

    StudentResponseDto update(Long id, StudentRequestDto studentRequestDto);

    StudentResponseDto findById(Long id);
}
