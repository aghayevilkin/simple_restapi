package com.example.demoproject.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@Entity
@ToString(exclude = "students")
@Table(name = "groups")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String details;

    @ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "teacher")
    private Teacher teacher;

    @ManyToMany(mappedBy = "groups", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<Student> students;

    // group remove address
}
