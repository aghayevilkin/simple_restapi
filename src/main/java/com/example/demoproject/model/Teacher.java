package com.example.demoproject.model;


import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@ToString(exclude = "groups")
@Table(name = "teachers")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;
    private String lessonName;
    private int age;
    private String phone;
    private String email;

    @OneToMany(mappedBy = "teacher")
    private List<Group> groups;

}
