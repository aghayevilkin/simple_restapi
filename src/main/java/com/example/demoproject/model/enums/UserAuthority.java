package com.example.demoproject.model.enums;

public enum UserAuthority {
    USER, ADMIN, SUPER_USER, ANONYMOUS, AGENT, PUBLISHER
}
