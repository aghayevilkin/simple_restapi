package com.example.demoproject.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Data
@Table(name = "authority")
public class Authority {

    @NotNull
    @Size(max = 100)
    @Id
    @Column(length = 100)
    private String authorityName;
}
