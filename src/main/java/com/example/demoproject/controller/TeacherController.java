package com.example.demoproject.controller;

import com.example.demoproject.dto.request.TeacherRequestDto;
import com.example.demoproject.dto.response.TeacherResponseDto;
import com.example.demoproject.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/teacher")
@RequiredArgsConstructor
public class TeacherController {

    private final TeacherService teacherService;

    @GetMapping("/{id}")
    public TeacherResponseDto getTeacher(@PathVariable("id") Long id) {
        return teacherService.findById(id);
    }

    @PostMapping
    public TeacherResponseDto createTeacher(@RequestBody @Valid TeacherRequestDto teacherDto) {
        return teacherService.createTeacher(teacherDto);
    }

    @DeleteMapping("/{id}")
    public void deleteTeacher(@PathVariable("id") Long id) {
        teacherService.deleteTeacher(id);
    }

    @PutMapping("/{id}")
    public TeacherResponseDto updateTeacher(@PathVariable("id") Long id, @RequestBody @Valid TeacherRequestDto teacherRequestDto) {
        return teacherService.update(id, teacherRequestDto);
    }
}
