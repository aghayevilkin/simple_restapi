package com.example.demoproject.controller;

import com.example.demoproject.dto.request.StudentRequestDto;
import com.example.demoproject.dto.response.StudentResponseDto;
import com.example.demoproject.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @GetMapping("/{id}")
    public StudentResponseDto getStudent(@PathVariable("id") Long id) {
        return studentService.findById(id);
    }

    @PostMapping
    public StudentResponseDto createStudent(@RequestBody @Valid StudentRequestDto studentRequestDto) {
        return studentService.createStudent(studentRequestDto);
    }

    @DeleteMapping("/{id}")
    public void deleteStudent(@PathVariable("id") Long id) {
        studentService.deleteStudent(id);
    }

    @PutMapping("/{id}")
    public StudentResponseDto updateStudent(@PathVariable("id") Long id,
                                            @RequestBody @Valid StudentRequestDto studentRequestDto) {
        return studentService.update(id, studentRequestDto);
    }
}
