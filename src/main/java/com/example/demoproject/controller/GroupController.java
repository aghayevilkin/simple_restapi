package com.example.demoproject.controller;

import com.example.demoproject.dto.request.GroupRequestDto;
import com.example.demoproject.dto.response.GroupResponseDto;
import com.example.demoproject.service.GroupService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/group")
@RequiredArgsConstructor
public class GroupController {

    private final GroupService groupService;

    @GetMapping("/{id}")
    public GroupResponseDto getGroup(@PathVariable("id") Long id) {
        return groupService.findById(id);
    }
    @PostMapping
    public GroupResponseDto createGroup(@RequestBody @Valid GroupRequestDto groupRequestDto) {
        return groupService.createGroup(groupRequestDto);
    }
    @DeleteMapping("/{id}")
    public void deleteGroup(@PathVariable("id") Long id) {
        groupService.deleteGroup(id);
    }

    @PutMapping("/{id}")
    public GroupResponseDto updateGroup(@PathVariable("id") Long id, @RequestBody @Valid GroupRequestDto groupDto) {
        return groupService.update(id, groupDto);

    }
}