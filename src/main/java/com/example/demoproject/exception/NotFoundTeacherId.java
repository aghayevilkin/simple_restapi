package com.example.demoproject.exception;

public class NotFoundTeacherId extends NotFoundException{
    private static final String MESSAGE = "Oops! No teacher with ID %s :(";

    public NotFoundTeacherId(Long id) {
        super(String.format(MESSAGE, id));
    }
}
