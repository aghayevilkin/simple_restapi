package com.example.demoproject.exception;

public class TeacherNotFound extends NotFoundException {

    private static final String MESSAGE = "Oops! No teacher with given found on the system :(" ;

    public TeacherNotFound() {
        super(MESSAGE);
    }
}
