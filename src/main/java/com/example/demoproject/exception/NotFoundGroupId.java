package com.example.demoproject.exception;

public class NotFoundGroupId extends NotFoundException{
    private static final String MESSAGE = "Oops! No group with ID %s :(";

    public NotFoundGroupId(Long id) {
        super(String.format(MESSAGE, id));
    }
}
