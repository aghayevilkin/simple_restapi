package com.example.demoproject.exception;

public class StudentNotFound extends NotFoundException {
    private static final String MESSAGE = "Oops! No student with given found on the system :(";

    public StudentNotFound() {
        super(MESSAGE);
    }
}
