package com.example.demoproject.exception;

public class GroupNotFound extends NotFoundException{
    private static final String MESSAGE = "Oops! No group with given found on the system :(";

    public GroupNotFound() {
        super(MESSAGE);
    }
}
