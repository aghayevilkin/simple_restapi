package com.example.demoproject.exception;

public class InvalidStateException extends RuntimeException {

    private static final long serialVersionUID = 2L;

    public InvalidStateException(String message) {
        super(message);
    }
}
