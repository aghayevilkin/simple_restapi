package com.example.demoproject.exception;

public class AuthorityAlreadyExistException extends InvalidStateException {

    private final static long serialVersionUID = 2L;

    public AuthorityAlreadyExistException(String name) {
        super(String.format("Authority \"%s\" already exist ", name));
    }
}

