package com.example.demoproject.repository;

import com.example.demoproject.model.Authority;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority,Long>,
        JpaSpecificationExecutor<Authority> {
    
    Optional<Authority> findByAuthorityNameIgnoreCase(String name);

}
