package com.example.demoproject.repository;



import com.example.demoproject.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long>,
        JpaSpecificationExecutor<User> {
    Optional<User> findUserByUsernameIgnoreCase(String username);

    Optional<User> findByUsername(String username);
}
