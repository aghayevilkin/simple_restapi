package com.example.demoproject.dto.response;

import lombok.Data;

@Data
public class GroupResponseDto {

    private Long id;
    private String name;
    private String details;
    private TeacherResponseDto teacher;
    private Long teacherId;

}
