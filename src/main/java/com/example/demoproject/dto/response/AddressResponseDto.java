package com.example.demoproject.dto.response;

import lombok.Data;

@Data
public class AddressResponseDto {

    private Long id;
    private String city;
    private String street;
}
