package com.example.demoproject.dto.response;

import lombok.Data;

@Data
public class UserResponseDto {

    private Long id;

    private String username;

    private String authority;
}
