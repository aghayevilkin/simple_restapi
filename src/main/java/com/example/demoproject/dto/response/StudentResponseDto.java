package com.example.demoproject.dto.response;

import com.example.demoproject.model.Group;
import lombok.Data;

import java.util.List;

@Data
public class StudentResponseDto {

    private Long id;
    private String firstName;
    private String lastName;
    private int age;
    private String email;
    private String phone;
    private List<GroupResponseDto> groupId;
    private AddressResponseDto address;


}
