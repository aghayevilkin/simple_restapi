package com.example.demoproject.dto.request;

import com.sun.istack.NotNull;
import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class GroupRequestDto {

    @NotNull
    @NotEmpty(message = "can't be empty")
    @Size(min = 1, max = 20)
    private String name;

    @NotNull
    @NotEmpty(message = "can't be empty")
    private String details;

    private Long teacherId;

}
