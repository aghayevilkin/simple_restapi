package com.example.demoproject.dto.request;


import com.sun.istack.NotNull;
import lombok.Data;
import javax.validation.constraints.NotEmpty;

@Data
public class AddressRequestDto {

    @NotNull
    @NotEmpty(message = "can't be empty")
    private String city;

    @NotNull
    @NotEmpty(message = "can't be empty")
    private String street;
}
