package com.example.demoproject.dto.request;

import com.sun.istack.NotNull;
import lombok.Data;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

@Data
public class TeacherRequestDto {

    @NotNull
    @NotEmpty(message = "can't be empty")
    private String firstName;

    @NotNull
    @NotEmpty(message = "can't be empty")
    private String lastName;

    @NotNull
    @NotEmpty(message = "can't be empty")
    private String lessonName;

    @Positive
    private int age;

    @NotEmpty
    private String phone;

    @Email
    private String e_mail;
}
