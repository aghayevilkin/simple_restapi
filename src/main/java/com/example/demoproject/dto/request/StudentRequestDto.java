package com.example.demoproject.dto.request;

import com.example.demoproject.dto.response.AddressResponseDto;
import lombok.Data;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
public class StudentRequestDto {

    @NotNull
    @NotEmpty(message = "can't be empty")
    private String firstName;

    @NotNull
    @NotEmpty(message = "can't be empty")
    private String lastName;

    @Positive
    private int age;

    @NotEmpty
    private String phone;

    @Email
    private String email;

    @NotNull
    private AddressRequestDto address;

    @NotNull
    private Long groupId;

}
