package com.example.demoproject.config;

import com.example.demoproject.model.enums.UserAuthority;
import com.example.demoproject.service.security.MyUserDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@RequiredArgsConstructor
@Configuration
public class SecurityConfigAdapter extends WebSecurityConfigurerAdapter {

    /** Domains **/
    private static final String GROUP_PATH = "/group/**";
    private static final String STUDENT_PATH = "/student/**";
    private static final String TEACHER_PATH = "/tecaher/**";
    private static final String USER_PATH = "/v1/user/**";

    /** Authority **/
    private static final String ADMIN = UserAuthority.ADMIN.name();
    private static final String USER = UserAuthority.USER.name();
    private static final String SUPER_USER = UserAuthority.SUPER_USER.name();

    @Bean
    public UserDetailsService userDetailsService() {
        return new MyUserDetailService();
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http
                .httpBasic()
                .and()
                .authorizeRequests()

                .antMatchers(HttpMethod.GET, GROUP_PATH).hasAnyAuthority(USER, ADMIN,SUPER_USER)
                .antMatchers(HttpMethod.POST, GROUP_PATH).hasAnyAuthority(ADMIN,SUPER_USER)
                .antMatchers(HttpMethod.DELETE, GROUP_PATH).hasAnyAuthority(ADMIN,SUPER_USER)
                .antMatchers(HttpMethod.PUT, GROUP_PATH).hasAnyAuthority(ADMIN,SUPER_USER)

                .antMatchers(HttpMethod.GET, STUDENT_PATH).hasAnyAuthority(USER, ADMIN,SUPER_USER)
                .antMatchers(HttpMethod.POST, STUDENT_PATH).hasAnyAuthority(ADMIN,SUPER_USER)
                .antMatchers(HttpMethod.DELETE, STUDENT_PATH).hasAnyAuthority(ADMIN,SUPER_USER)
                .antMatchers(HttpMethod.PUT, STUDENT_PATH).hasAnyAuthority(ADMIN,SUPER_USER)

                .antMatchers(HttpMethod.GET, USER_PATH).hasAnyAuthority(ADMIN, SUPER_USER)
                .antMatchers(HttpMethod.POST, USER_PATH).hasAuthority(SUPER_USER)
                .antMatchers(HttpMethod.DELETE, USER_PATH).hasAuthority(SUPER_USER)
                .antMatchers(HttpMethod.PUT, USER_PATH).hasAnyAuthority(SUPER_USER)

                .antMatchers(HttpMethod.GET, TEACHER_PATH).hasAnyAuthority(USER, ADMIN,SUPER_USER)
                .antMatchers(HttpMethod.POST, TEACHER_PATH).hasAnyAuthority(ADMIN,SUPER_USER)
                .antMatchers(HttpMethod.DELETE, TEACHER_PATH).hasAnyAuthority(ADMIN,SUPER_USER)
                .antMatchers(HttpMethod.PUT, TEACHER_PATH).hasAnyAuthority(ADMIN,SUPER_USER)
                .anyRequest().authenticated()
                .and().csrf().disable()
                .formLogin().disable();
    }
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication().withUser("admin").
//                password(bCryptPasswordEncoder().encode("12345")).roles(SUPER_USER);
        auth
                .userDetailsService(userDetailsService())
                .passwordEncoder(bCryptPasswordEncoder());
    }

}
