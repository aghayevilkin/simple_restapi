package com.example.demoproject;

import com.example.demoproject.service.TestService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@RequiredArgsConstructor
public class DemoProjectApplication implements CommandLineRunner {

    private final TestService testService;
    public static void main(String[] args) {
        SpringApplication.run(DemoProjectApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
//        testService.createtest();
//        testService.bridge();
        log.info("creaating students and addreses");
    }
}
